package com.nvd.models.listpopular

import java.io.Serializable

data class ResultMovies(val release_date : Int,
                        val original_title : String,
                        val overview : String,
                        val vote_average : Double) : Serializable {
}