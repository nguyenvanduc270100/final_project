package com.nvd.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.nvd.activities.R
import com.shashank.sony.fancytoastlib.FancyToast

class AboutFragment : Fragment() {
    lateinit var mAboutTMDB : TextView
    lateinit var mOurHistory : TextView
    lateinit var mStayInTouch : TextView
    lateinit var mLogos : TextView
    lateinit var mApi : TextView
    lateinit var mEditContent : TextView
    lateinit var mFAQ : TextView
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_about, container, false)
        mAboutTMDB = view.findViewById(R.id.about_tmdb)
        mOurHistory = view.findViewById(R.id.our_history)
        mStayInTouch = view.findViewById(R.id.staying_in_touch)
        mLogos = view.findViewById(R.id.logos_attribution)
        mApi = view.findViewById(R.id.api)
        mEditContent = view.findViewById(R.id.editing_content)
        mFAQ = view.findViewById(R.id.faq)

        mAboutTMDB.setOnClickListener {
            FancyToast.makeText(requireContext(), "about tmdb", FancyToast.LENGTH_SHORT, FancyToast.INFO, true).show()
        }

        mOurHistory.setOnClickListener {
            FancyToast.makeText(requireContext(), "Our history", FancyToast.LENGTH_SHORT, FancyToast.INFO, true).show()
        }

        mStayInTouch.setOnClickListener {
            FancyToast.makeText(requireContext(), "Stayign in touch", FancyToast.LENGTH_SHORT, FancyToast.INFO, true).show()
        }

        mLogos.setOnClickListener {
            FancyToast.makeText(requireContext(), "Logos", FancyToast.LENGTH_SHORT, FancyToast.INFO, true).show()
        }

        mApi.setOnClickListener {
            FancyToast.makeText(requireContext(), "API", FancyToast.LENGTH_SHORT, FancyToast.INFO, true).show()
        }

        mEditContent.setOnClickListener {
            FancyToast.makeText(requireContext(), "edit content", FancyToast.LENGTH_SHORT, FancyToast.INFO, true).show()
        }

        mFAQ.setOnClickListener {
            FancyToast.makeText(requireContext(), "FAQ", FancyToast.LENGTH_SHORT, FancyToast.INFO, true).show()
        }
        return view
    }
}