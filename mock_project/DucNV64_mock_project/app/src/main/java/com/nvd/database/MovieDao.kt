package com.nvd.database

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.nvd.database.MovieDB

@Dao
interface MovieDao {
    @Query("SELECT * FROM MovieDB")
    fun getAll(): List<MovieDB>

    @Query("SELECT * FROM MovieDB WHERE uid IN (:userIds)")
    fun loadAllByIds(userIds: IntArray): List<MovieDB>

    @Query("SELECT * FROM MovieDB WHERE name LIKE :first AND " +
            "name LIKE :last LIMIT 1")
    fun findByName(first: String, last: String): MovieDB

    @Insert
    fun insertAll(vararg users: MovieDB)

    @Delete
    fun delete(user: MovieDB)
}