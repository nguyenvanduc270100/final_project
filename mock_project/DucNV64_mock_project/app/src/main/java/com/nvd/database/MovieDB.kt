package com.nvd.database

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class MovieDB(@PrimaryKey val uid: Int, val name : String, val imgTitle : Int, val description : String) {
}