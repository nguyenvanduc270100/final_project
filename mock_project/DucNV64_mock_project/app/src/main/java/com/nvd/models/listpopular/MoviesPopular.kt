package com.nvd.models.listpopular

import java.io.Serializable

data class MoviesPopular(val page : Int, val result : List<ResultMovies>, var isFeature : Boolean = true,
                         var isLike : Boolean = true) : Serializable {
}