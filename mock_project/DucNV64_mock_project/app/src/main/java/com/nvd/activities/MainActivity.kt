package com.nvd.activities

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.PopupMenu
import androidx.appcompat.widget.Toolbar
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationBarView
import com.google.android.material.navigation.NavigationView
import com.nvd.fragments.AboutFragment
import com.nvd.fragments.FavoriteFragment
import com.nvd.fragments.MoviesFragment
import com.nvd.fragments.SettingFragment


class MainActivity : AppCompatActivity() {
    var mDrawerLayout : DrawerLayout? = null
    var mToolBar : Toolbar? = null
    var mNavigationView : NavigationView? = null
    var mBottomNavigation : BottomNavigationView? = null
    var mTitle : TextView? = null
    var mChangeLayoutBnt : ImageButton? = null
    var mOptionMenu : ImageButton? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mDrawerLayout = findViewById(R.id.drawer_layout)
        mToolBar = findViewById(R.id.toolbar)
        mBottomNavigation = findViewById(R.id.bottom_navi)
        mNavigationView = findViewById(R.id.navigation_view)
        mTitle = findViewById(R.id.main_title)
        mChangeLayoutBnt = findViewById(R.id.change_layout)
        mOptionMenu = findViewById(R.id.option_menu)


        //setSupportActionBar(toolbar);
        val actionBarDrawerToggle = ActionBarDrawerToggle(
            this,
            mDrawerLayout, mToolBar, R.string.drawer_open, R.string.drawer_close
        )

        mDrawerLayout!!.addDrawerListener(actionBarDrawerToggle)
        actionBarDrawerToggle.syncState()

        mNavigationView!!.setNavigationItemSelectedListener(NavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.drawer_email -> Toast.makeText(
                    this@MainActivity,
                    "drawer_email",
                    Toast.LENGTH_SHORT
                ).show()
                R.id.drawer_birthday -> Toast.makeText(
                    this@MainActivity,
                    "drawer_birthday",
                    Toast.LENGTH_SHORT
                ).show()
                R.id.drawer_sex -> Toast.makeText(
                    this@MainActivity,
                    "drawer_sex",
                    Toast.LENGTH_SHORT
                ).show()
                R.id.drawer_reminder -> Toast.makeText(
                    this@MainActivity,
                    "drawer_reminder",
                    Toast.LENGTH_SHORT
                ).show()

            }
            mDrawerLayout!!.closeDrawer(GravityCompat.START)
            true
        })

        mBottomNavigation!!.setOnItemSelectedListener(NavigationBarView.OnItemSelectedListener { item ->
            val id = item.itemId
            when (id) {
                R.id.movies -> {
                    val movieFragment = MoviesFragment()
                    replaceFragment(movieFragment)
                    mTitle!!.text = resources.getString(R.string.title_movies)
                }
                R.id.favorite -> {
                    val favouriteFragment = FavoriteFragment()
                    replaceFragment(favouriteFragment)
                    mTitle!!.text = resources.getString(R.string.title_favorite)
                }
                R.id.setting -> {
                    val settingFragment = SettingFragment()
                    replaceFragment(settingFragment)
                    mTitle!!.text = resources.getString(R.string.title_setting)
                }
                R.id.about -> {
                    val aboutFragment = AboutFragment()
                    replaceFragment(aboutFragment)
                    mTitle!!.text = resources.getString(R.string.title_about)
                }
            }
            true
        })

        mChangeLayoutBnt!!.setOnClickListener {

        }

        mOptionMenu!!.setOnClickListener {
            val popupMenu = PopupMenu(this, mOptionMenu!!)
            popupMenu.menuInflater.inflate(R.menu.option_menu, popupMenu.menu)
            popupMenu.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener { item: MenuItem? ->

                when (item!!.itemId) {
                    R.id.language -> {
                        Toast.makeText(this@MainActivity, item.title, Toast.LENGTH_SHORT).show()
                    }
                    R.id.light -> {
                        Toast.makeText(this@MainActivity, item.title, Toast.LENGTH_SHORT).show()
                    }
                    R.id.announce -> {
                        Toast.makeText(this@MainActivity, item.title, Toast.LENGTH_SHORT).show()
                    }

                    R.id.speed -> {
                        Toast.makeText(this@MainActivity, item.title, Toast.LENGTH_SHORT).show()
                    }
                }

                true
            })
            popupMenu.show()
        }
    }

    override fun onBackPressed() {
        if (mDrawerLayout!!.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout!!.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    fun replaceFragment(fragment : Fragment){
        val transaction  = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.content_frame, fragment)
        transaction.commit()
    }


}