package com.nvd.fragments

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.*
import android.widget.*
import androidx.fragment.app.Fragment
import com.nvd.activities.R
import com.shashank.sony.fancytoastlib.FancyToast

class SettingFragment : Fragment() {
    lateinit var mLinearCategory : LinearLayout
    lateinit var mLinearMovieRate : LinearLayout
    lateinit var mLinearFromRelease : LinearLayout
    lateinit var mLinearSortBy : LinearLayout
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_setting, container, false)

        mLinearCategory = view.findViewById(R.id.category)
        mLinearCategory.setOnClickListener {
            openDialog(Gravity.CENTER, "Category")
        }

        mLinearMovieRate = view.findViewById(R.id.movie_rate)
        mLinearMovieRate.setOnClickListener {
            openRateDialog(Gravity.CENTER)
        }

        mLinearFromRelease = view.findViewById(R.id.from_release)
        mLinearFromRelease.setOnClickListener {
            openReleaseDialog(Gravity.CENTER)
        }

        mLinearSortBy = view.findViewById(R.id.sort_by)
        mLinearSortBy.setOnClickListener {
            openDialog(Gravity.CENTER, "Sort by")
        }
        return view
    }

    private fun openDialog(gravity: Int, title : String) {
        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.layout_dialog)
        val window = dialog.window
        if (window == null) {
            return
        } else {
            window.setLayout(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT
            )
            window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            val windowAttribute = window.attributes
            windowAttribute.gravity = gravity
            window.attributes = windowAttribute
            if (Gravity.BOTTOM == gravity) {
                dialog.setCancelable(true)
            } else {
                dialog.setCancelable(false)
            }

            var mTitleDialog = dialog.findViewById<TextView>(R.id.title_dialog)
            mTitleDialog.text = title

            if (title.equals("Category")){
                val mRadioGroup = dialog.findViewById<RadioGroup>(R.id.radio_group)
                val mRadioSort = dialog.findViewById<RadioGroup>(R.id.radio_sort)
                mRadioGroup.visibility = View.VISIBLE
                mRadioSort.visibility  = View.GONE
                mRadioGroup.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { radioGroup, i ->
                    when(i){
                        R.id.popular_movie -> {
                            FancyToast.makeText(requireContext(), "popular movie", FancyToast.LENGTH_SHORT, FancyToast.INFO, true).show()
                        }
                        R.id.top_rate_movie -> {
                            FancyToast.makeText(requireContext(), "top_rate_movie", FancyToast.LENGTH_SHORT, FancyToast.INFO, true).show()

                        }
                        R.id.upcoming_movie -> {
                            FancyToast.makeText(requireContext(), "upcoming_movie", FancyToast.LENGTH_SHORT, FancyToast.INFO, true).show()

                        }
                        R.id.now_playing_movie -> {
                            FancyToast.makeText(requireContext(), "now_playing_movie", FancyToast.LENGTH_SHORT, FancyToast.INFO, true).show()

                        }

                    }
                })

            }

            if (title.equals("Sort by")){
                val mRadioGroup = dialog.findViewById<RadioGroup>(R.id.radio_group)
                val mRadioSort = dialog.findViewById<RadioGroup>(R.id.radio_sort)
                mRadioGroup.visibility = View.GONE
                mRadioSort.visibility  = View.VISIBLE
                mRadioSort.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { radioGroup, i ->
                    when(i){
                        R.id.release_date -> {
                            FancyToast.makeText(requireContext(), "release_date", FancyToast.LENGTH_SHORT, FancyToast.INFO, true).show()
                        }
                        R.id.rating -> {
                            FancyToast.makeText(requireContext(), "rating", FancyToast.LENGTH_SHORT, FancyToast.INFO, true).show()

                        }

                    }
                })
            }

            val cancel = dialog.findViewById<Button>(R.id.cancel)
            val ok = dialog.findViewById<Button>(R.id.ok)
            cancel.setOnClickListener { dialog.dismiss() }
            ok.setOnClickListener {
                FancyToast.makeText(
                    requireContext(),
                    "editText.text",
                    FancyToast.LENGTH_LONG,
                    FancyToast.INFO,
                    true
                ).show()
                dialog.dismiss()
            }
            dialog.show()

        }
    }

    private fun openReleaseDialog(gravity: Int) {
        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.layout_release_dialog)
        val window = dialog.window
        if (window == null) {
            return
        } else {
            window.setLayout(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT
            )
            window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            val windowAttribute = window.attributes
            windowAttribute.gravity = gravity
            window.attributes = windowAttribute
            if (Gravity.BOTTOM == gravity) {
                dialog.setCancelable(true)
            } else {
                dialog.setCancelable(false)
            }
            val editText = dialog.findViewById<EditText>(R.id.edt_input)
            val cancel = dialog.findViewById<Button>(R.id.cancel)
            val ok = dialog.findViewById<Button>(R.id.ok)
            cancel.setOnClickListener { dialog.dismiss() }
            ok.setOnClickListener {
                FancyToast.makeText(
                    requireContext(),
                    editText.text,
                    FancyToast.LENGTH_LONG,
                    FancyToast.INFO,
                    true
                ).show()
                dialog.dismiss()
            }
            dialog.show()
        }
    }

    private fun openRateDialog(gravity: Int) {
        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.layout_rate_dialog)
        val window = dialog.window
        if (window == null) {
            return
        } else {
            window.setLayout(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT
            )
            window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            val windowAttribute = window.attributes
            windowAttribute.gravity = gravity
            window.attributes = windowAttribute
            if (Gravity.BOTTOM == gravity) {
                dialog.setCancelable(true)
            } else {
                dialog.setCancelable(false)
            }
            val editText = dialog.findViewById<SeekBar>(R.id.edt_input)
            editText.max = 100

            val cancel = dialog.findViewById<Button>(R.id.cancel)
            val ok = dialog.findViewById<Button>(R.id.ok)
            cancel.setOnClickListener { dialog.dismiss() }
            ok.setOnClickListener {
                FancyToast.makeText(
                    requireContext(),
                    editText.progress.toString(),
                    FancyToast.LENGTH_LONG,
                    FancyToast.INFO,
                    true
                ).show()
                dialog.dismiss()
            }
            dialog.show()
        }
    }
}