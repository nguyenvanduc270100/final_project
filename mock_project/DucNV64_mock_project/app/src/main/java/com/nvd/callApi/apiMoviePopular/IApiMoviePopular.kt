package com.nvd.callApi.apiMoviePopular

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.nvd.models.listpopular.MoviesPopular
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import retrofit2.http.GET
import retrofit2.http.Query

interface IApiMoviePopular {
    /*api.themoviedb.org/3/movie/popular?
              api_key=e7631ffcb8e766993e5ec0c1f4245f93
              &page={pageNumber}
     */

    companion object{
        val gson = GsonBuilder()
            .setDateFormat("yyyy-MM-dd HH:mm:ss")
            .create()

        val iApiMoviePopular = Retrofit.Builder()
            .baseUrl("api.themoviedb.org/3/movie/")
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
            .create(IApiMoviePopular::class.java)
    }

    @GET("popular")
    fun  listMoviePopular(@Query("api_key") api_key: String ) : Call<MoviesPopular>
}