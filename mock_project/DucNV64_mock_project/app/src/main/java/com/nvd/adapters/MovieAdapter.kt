package com.nvd.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.nvd.activities.R
import com.nvd.models.listpopular.MoviesPopular

class MovieAdapter(val ct : Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>(){
    companion object{
        private const val TYPE_MOVIE_LINEAR = 1
        private const val TYPE_MOVIE_GRID = 2
    }
    class MovieLinearViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        var mImgTitle = itemView.findViewById<ImageView>(R.id.img_title_movie)
        var mRealeaseTxt = itemView.findViewById<TextView>(R.id.release_date)
        var mRateTxt = itemView.findViewById<TextView>(R.id.rating)
        var mDescription = itemView.findViewById<TextView>(R.id.description)
        var mLikeBtn = itemView.findViewById<ImageButton>(R.id.like_btn)
    }

    class MovieGridViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        var mTitleImg = itemView.findViewById<ImageView>(R.id.title_img)
        var mNameMovie = itemView.findViewById<TextView>(R.id.name_movie)
    }


    private lateinit var mListMovie : ArrayList<MoviesPopular>

    fun setData(list : ArrayList<MoviesPopular>){
        this.mListMovie = list
        notifyDataSetChanged()
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (TYPE_MOVIE_LINEAR == viewType) {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_movies_linear, parent, false)
            return MovieLinearViewHolder(view)
        } else if (TYPE_MOVIE_GRID == viewType) {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_movie_grid, parent, false)
            return MovieGridViewHolder(view)
        }

        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_movies_linear, parent, false)
        return MovieLinearViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val movie = mListMovie.get(position)
        if (movie == null) return

        if (TYPE_MOVIE_LINEAR == holder.itemViewType){
            val mMovieLinearViewHolder = holder as MovieLinearViewHolder
            mMovieLinearViewHolder.mImgTitle.setImageResource(movie.page)
            val rs = movie.result.get(position)
            mMovieLinearViewHolder.mRealeaseTxt.text = rs.release_date.toString()
            mMovieLinearViewHolder.mRateTxt.text = rs.vote_average.toString()
            mMovieLinearViewHolder.mDescription.text = rs.overview
            mMovieLinearViewHolder.mLikeBtn.setOnClickListener {
                if (movie.isLike == true){
                    Toast.makeText(ct, "Liked", Toast.LENGTH_SHORT).show()
                    mMovieLinearViewHolder.mLikeBtn.background = ct.getDrawable(R.drawable.ic_like)
                } else{
                    Toast.makeText(ct, "DisLiked", Toast.LENGTH_SHORT).show()
                    mMovieLinearViewHolder.mLikeBtn.background = ct.getDrawable(R.drawable.ic_dislike)
                }

            }
        } else if (TYPE_MOVIE_GRID == holder.itemViewType){
            val mMovieGridViewHolder = holder as MovieGridViewHolder
            mMovieGridViewHolder.mTitleImg.setImageResource(movie.page)
            val rs = movie.result.get(position)
            mMovieGridViewHolder.mNameMovie.text = rs.release_date.toString()

        }
    }

    override fun getItemCount(): Int {
        if (mListMovie != null){
            return mListMovie.size
        }

        return 0
    }

    override fun getItemViewType(position: Int): Int {
        val user  = mListMovie.get(position)
        if (true == user.isFeature){
            return TYPE_MOVIE_LINEAR
        } else if (false == user.isFeature){
            return TYPE_MOVIE_GRID
        }

        return 0
    }
}